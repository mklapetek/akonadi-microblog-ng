/*
   Based on akonadi-facebook SettingsDialog
   Copyright 2010 Thomas McGuire <mcguire@kde.org>
   Copyright 2012 Martin Klapetek <martin.klapetek@gmail.com>

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2 of the License or
   ( at your option ) version 3 or, at the discretion of KDE e.V.
   ( which shall act as a proxy as in section 14 of the GPLv3 ), any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/
#include "settingsdialog.h"
#include "microblogresource.h"
#include "settings.h"

#include <KAboutApplicationDialog>
#include <KAboutData>
#include <KWindowSystem>

#define RESOURCE_VERSION "0.1"

using namespace Akonadi;

SettingsDialog::SettingsDialog( MicroblogResource *parentResource, KOAuth::KOAuth *authHelper, WId parentWindow )
  : KDialog(),
    mParentResource( parentResource ),
    mAuthHelper( authHelper ),
    mTriggerSync( false )
{
  KWindowSystem::setMainWindow( this, parentWindow );
  setButtons( Ok|Cancel|User1 );
  setButtonText( User1, i18n( "About" ) );
  setButtonIcon( User1, KIcon( "help-about" ) );
  setWindowIcon( KIcon( "microblogresource" ) ); //FIXME get an icon
  setWindowTitle( i18n( "Microblog Settings" ) );

  setupWidgets();
  loadSettings();
}

SettingsDialog::~SettingsDialog()
{
  if ( mTriggerSync ) {
    mParentResource->synchronize();
  }
}

void SettingsDialog::setupWidgets()
{
  QWidget * const page = new QWidget( this );
  setupUi( page );
  setMainWidget( page );
  serviceType->addItem( "Twitter", "https://api.twitter.com/1/" );
  serviceType->addItem( "Identi.ca", "http://identi.ca/api/" );
  busyWidget->hide();

//   connect( resetButton, SIGNAL(clicked(bool)), this, SLOT(resetAuthentication()) );
  connect( authenticateButton, SIGNAL( clicked( bool ) ), this, SLOT( showAuthenticationDialog() ) );
}

void SettingsDialog::showAuthenticationDialog()
{
    mAuthHelper->setUser( nameEdit->text() );
    mAuthHelper->setPassword( passEdit->text() );
    mAuthHelper->setServiceBaseUrl( serviceType->itemData( serviceType->currentIndex() ).toString() );

    connect(mAuthHelper, SIGNAL( statusUpdated( QString, QString, QString, QString) ),
            SLOT( authorizationStatusUpdated( QString, QString, QString, QString) ) );
    connect(mAuthHelper, SIGNAL( authorized() ),
            this, SLOT( authorizationDone() ) );
    // Run start() instead of run() here to move to another thread.
    // as we can't share pixmap, this won't work using an invisible webkit
    //authHelper->start();
    mAuthHelper->run();
}

void SettingsDialog::authenticationCanceled()
{
  authenticateButton->setEnabled( true );
}

void SettingsDialog::resetAuthentication()
{
//   Settings::self()->setAccessToken( QString() );
//   Settings::self()->setUserName( QString() );
//   updateAuthenticationWidgets();
}

void SettingsDialog::loadSettings()
{
  if ( mParentResource->name() == mParentResource->identifier() )
    mParentResource->setName( i18n( "Microblog" ) );

  nameEdit->setText( Settings::self()->username() );
  nameEdit->setFocus();
}

void SettingsDialog::saveSettings()
{
  QString resourceName = QString( "%1 (%2)" ).arg( serviceType->itemText( serviceType->currentIndex() ), nameEdit->text() );
  mParentResource->setName( resourceName );
  Settings::setServiceBaseUrl( serviceType->itemData( serviceType->currentIndex() ).toString() );
  Settings::setUsername( nameEdit->text() );
  Settings::self()->writeConfig();
}

void SettingsDialog::slotButtonClicked( int button )
{
  switch( button ) {
    case Ok:
      saveSettings();
      accept();
      break;
    case Cancel:
      reject();
      return;
    case User1: {
      KAboutData aboutData( QByteArray( "akonadi_microblogng_resource" ),
                            QByteArray(),
                            ki18n( "Akonadi Microblog Resource" ),
                            QByteArray( RESOURCE_VERSION ),
                            ki18n( "Makes your Twitter/Identi.ca timelines available in KDE via Akonadi." ),
                            KAboutData::License_GPL_V2,
                            ki18n( "Copyright (C) 2010,2011 Microblog Plasma Dataengine Developers,\n"
                                   "Copyright (C) 2012 Akonadi Microblog Resource Developers" ) );
      aboutData.addAuthor( ki18n( "Martin Klapetek" ), ki18n( "Developer" ), "martin.klapetek@gmail.com" );
      aboutData.setProgramIconName( "microblogresource" ); //FIXME get an icon
      aboutData.setTranslator( ki18nc( "NAME OF TRANSLATORS", "Your names" ),
                            ki18nc( "EMAIL OF TRANSLATORS", "Your emails" ) );
      KAboutApplicationDialog *dialog = new KAboutApplicationDialog( &aboutData, this );
      dialog->setAttribute( Qt::WA_DeleteOnClose, true );
      dialog->show();
      break;
    }
  }
}

void SettingsDialog::authorizationDone()
{
    saveSettings();
    accept();
}

void SettingsDialog::authorizationStatusUpdated( const QString &user, const QString &serviceBaseUrl, const QString &status, const QString &message)
{
    kDebug() << user << serviceBaseUrl << status << message;
    if ( status == "Busy" ) {
        authenticateButton->setDisabled( true );
        nameEdit->setDisabled( true );
        passEdit->setDisabled( true );
        serviceType->setDisabled( true );
        busyWidget->show();
    } else if ( status == "Ok" || status == "Error" ) {
        authenticateButton->setEnabled( true );
        nameEdit->setEnabled( true );
        passEdit->setEnabled( true );
        serviceType->setEnabled( true );
        busyWidget->hide();
    }

    statusLabel->setText( message );
}

#include "settingsdialog.moc"
