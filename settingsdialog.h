/*
   Based on akonadi-facebook SettingsDialog
   Copyright 2010 Thomas McGuire <mcguire@kde.org>
   Copyright 2012 Martin Klapetek <martin.klapetek@gmail.com>

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2 of the License or
   ( at your option ) version 3 or, at the discretion of KDE e.V.
   ( which shall act as a proxy as in section 14 of the GPLv3 ), any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef MICROBLOG_SETTINGSDIALOG_H
#define MICROBLOG_SETTINGSDIALOG_H

#include "ui_settingsdialog.h"

namespace KOAuth
{
class KOAuth;
}

class MicroblogResource;
class KJob;

class SettingsDialog : public KDialog, private Ui::SettingsDialog
{
  Q_OBJECT

  public:
    SettingsDialog( MicroblogResource *parentResource, KOAuth::KOAuth *authHelper, WId parentWindow );
    ~SettingsDialog();

  private slots:
    virtual void slotButtonClicked( int button );
    void resetAuthentication();
    void showAuthenticationDialog();
    void authenticationCanceled();

    void authorizationDone();
    void authorizationStatusUpdated( const QString &user, const QString &serviceBaseUrl, const QString &status, const QString &message = QString() );

  private:
    void setupWidgets();
    void loadSettings();
    void saveSettings();

    MicroblogResource *mParentResource;
    bool mTriggerSync;
    KOAuth::KOAuth *mAuthHelper;
};

#endif
