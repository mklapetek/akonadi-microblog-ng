/*
   Copyright 2012 Martin Klapetek <martin.klapetek@gmail.com>

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2 of the License or
   ( at your option ) version 3 or, at the discretion of KDE e.V.
   ( which shall act as a proxy as in section 14 of the GPLv3 ), any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef MICROBLOGRESOURCE_H
#define MICROBLOGRESOURCE_H

#include <akonadi/resourcebase.h>
#include <Akonadi/SocialUtils/SocialFeedItem>

#include <libkoauth/koauth.h>

#include "libkmicroblog/statusinfo.h"

namespace Akonadi
{
class ImageProvider;
}

class MicroblogResource : public Akonadi::ResourceBase,
                          public Akonadi::AgentBase::Observer
{
    Q_OBJECT

public:
    MicroblogResource( const QString &id );
    ~MicroblogResource();

    using ResourceBase::synchronize;

public Q_SLOTS:
    virtual void configure( WId windowId );
    Q_SCRIPTABLE void configureByAccount( int accountId );

protected Q_SLOTS:
    void retrieveCollections();
    void retrieveItems( const Akonadi::Collection &col );
    bool retrieveItem( const Akonadi::Item &item, const QSet<QByteArray> &parts );

    void itemAdded( const Akonadi::Item &item, const Akonadi::Collection &collection );
private Q_SLOTS:
    void onAbortRequested();
    void timelineJobFinished( KJob *job );
    void statusAddJobFinished( KJob *job );

protected:
    virtual void aboutToQuit();

    virtual void itemChanged( const Akonadi::Item &item, const QSet<QByteArray> &parts );
    virtual void itemRemoved( const Akonadi::Item &item );

private:
    Akonadi::SocialFeedItem convertToSocialFeedItem( const KMicroblog::StatusInfoPtr &statusInfo );

    QWeakPointer<KOAuth::KOAuth> m_authHelper;
    QList< QWeakPointer<KJob> > m_currentJobs;
    bool m_idle;
    Akonadi::ImageProvider *m_avatarProvider;

};

#endif
