/*
   Copyright 2012 Martin Klapetek <martin.klapetek@gmail.com>

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2 of the License or
   ( at your option ) version 3 or, at the discretion of KDE e.V.
   ( which shall act as a proxy as in section 14 of the GPLv3 ), any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/


#ifndef KMICROBLOG_USERINFO_H
#define KMICROBLOG_USERINFO_H

#include <QObject>
#include <QUrl>
#include <QSharedPointer>
#include <QMetaType>

#include "libkmicroblog_export.h"

namespace KMicroblog {

class LIBKMICROBLOG_EXPORT UserInfo : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name WRITE setName READ name)
    Q_PROPERTY(QString created_at WRITE setCreatedAt READ createdAt)
    Q_PROPERTY(QUrl profile_image_url WRITE setProfileImageUrl READ profileImageUrl)
    Q_PROPERTY(QString location WRITE setLocation READ location)
    Q_PROPERTY(QString url WRITE setUrl READ url)
    Q_PROPERTY(qulonglong favorites_count WRITE setFavoritesCount READ favoritesCount)
    Q_PROPERTY(QString utc_offset WRITE setUtcOffset READ utcOffset)
    Q_PROPERTY(QString id WRITE setId READ id)
//    Q_PROPERTY(bool protected WRITE setProtected READ protected) //FIXME 'protected' confuses compiler
    Q_PROPERTY(qulonglong followers_count WRITE setFollowersCount READ followersCount)
    Q_PROPERTY(QString lang WRITE setLang READ lang)
    Q_PROPERTY(bool verified WRITE setVerified READ verified)
    Q_PROPERTY(QString description WRITE setDescription READ description)
    Q_PROPERTY(qulonglong friends_count WRITE setFriendsCount READ friendsCount)
    Q_PROPERTY(qulonglong statuses_count WRITE setStatusesCount READ statusesCount)
    Q_PROPERTY(bool following WRITE setFollowing READ following)
    Q_PROPERTY(QString screen_name WRITE setScreenName READ screenName)

public:
    void setName(const QString &name);
    QString name() const;

    void setCreatedAt(const QString &createdAt);
    QString createdAt() const;

    void setProfileImageUrl(const QUrl &url);
    QUrl profileImageUrl() const;

    void setLocation(const QString &location);
    QString location() const;

    void setUrl(const QString &url);
    QString url() const;

    void setFavoritesCount(qulonglong count);
    qulonglong favoritesCount() const;

    void setUtcOffset(const QString &utcOffset);
    QString utcOffset() const;

    void setId(const QString &id);
    QString id() const;

    void setFollowersCount(qulonglong count);
    qulonglong followersCount() const;

    void setLang(const QString &lang);
    QString lang() const;

    void setVerified(bool verified);
    bool verified() const;

    void setDescription(const QString &description);
    QString description() const;

    void setFriendsCount(qulonglong count);
    qulonglong friendsCount() const;

    void setStatusesCount(qulonglong count);
    qulonglong statusesCount() const;

    void setFollowing(bool following);
    bool following() const;

    void setScreenName(const QString &screenName);
    QString screenName() const;

private:
    QString m_name;
    QString m_createdAt;
    QUrl m_profileImageUrl;
    QString m_location;
    QString m_url;
    qulonglong m_favoritesCount;
    QString m_utcOffset;
    QString m_id;
    qulonglong m_followersCount;
    QString m_lang;
    bool m_verified;
    QString m_description;
    qulonglong m_friendsCount;
    qulonglong m_statusesCount;
    bool m_following;
    QString m_screenName;
};

typedef QSharedPointer<UserInfo> UserInfoPtr;

}

Q_DECLARE_METATYPE(KMicroblog::UserInfo *);



#endif // KMICROBLOG_USERINFO_H
