/*
   Copyright 2012 Martin Klapetek <martin.klapetek@gmail.com>

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2 of the License or
   ( at your option ) version 3 or, at the discretion of KDE e.V.
   ( which shall act as a proxy as in section 14 of the GPLv3 ), any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef KMICROBLOG_TIMELINELISTJOB_H
#define KMICROBLOG_TIMELINELISTJOB_H

#include "microblogjob.h"
#include "statusinfo.h"

#include <QVariant>

#include "libkmicroblog_export.h"

namespace KMicroblog {

class LIBKMICROBLOG_EXPORT TimelineListJob : public MicroblogGetJob
{
    Q_OBJECT
public:
    TimelineListJob(const QString &timelinePath, KOAuth::KOAuth *authHelper);

    QList<StatusInfoPtr> statuses() const;
    int numEntries() const;

protected:
    void handleData(const QVariant &data);

private:
    QList<StatusInfoPtr> m_posts;
};

}

#endif // KMICROBLOG_TIMELINELISTJOB_H
