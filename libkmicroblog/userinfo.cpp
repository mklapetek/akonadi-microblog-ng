/*
   Copyright 2012 Martin Klapetek <martin.klapetek@gmail.com>

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2 of the License or
   ( at your option ) version 3 or, at the discretion of KDE e.V.
   ( which shall act as a proxy as in section 14 of the GPLv3 ), any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/


#include "userinfo.h"

void KMicroblog::UserInfo::setName(const QString &name)
{
    m_name = name;
}

QString KMicroblog::UserInfo::name() const
{
    return m_name;
}

void KMicroblog::UserInfo::setCreatedAt(const QString &createdAt)
{
    m_createdAt = createdAt;
}

QString KMicroblog::UserInfo::createdAt() const
{
    return m_createdAt;
}

void KMicroblog::UserInfo::setProfileImageUrl(const QUrl &url)
{
    m_profileImageUrl = url;
}

QUrl KMicroblog::UserInfo::profileImageUrl() const
{
    return m_profileImageUrl;
}

void KMicroblog::UserInfo::setLocation(const QString &location)
{
    m_location = location;
}

QString KMicroblog::UserInfo::location() const
{
    return m_location;
}

void KMicroblog::UserInfo::setUrl(const QString &url)
{
    m_url = url;
}

QString KMicroblog::UserInfo::url() const
{
    return m_url;
}

void KMicroblog::UserInfo::setFavoritesCount(qulonglong count)
{
    m_favoritesCount = count;
}

qulonglong KMicroblog::UserInfo::favoritesCount() const
{
    return m_favoritesCount;
}

void KMicroblog::UserInfo::setUtcOffset(const QString &utcOffset)
{
    m_utcOffset = utcOffset;
}

QString KMicroblog::UserInfo::utcOffset() const
{
    return m_utcOffset;
}

void KMicroblog::UserInfo::setId(const QString &id) {
    m_id = id;
}

QString KMicroblog::UserInfo::id() const
{
    return m_id;
}

void KMicroblog::UserInfo::setFollowersCount(qulonglong count)
{
    m_followersCount = count;
}

qulonglong KMicroblog::UserInfo::followersCount() const
{
    return m_followersCount;
}

void KMicroblog::UserInfo::setLang(const QString &lang)
{
    m_lang = lang;
}

QString KMicroblog::UserInfo::lang() const
{
    return m_lang;
}

void KMicroblog::UserInfo::setVerified(bool verified)
{
    m_verified = verified;
}

bool KMicroblog::UserInfo::verified() const
{
    return m_verified;
}

void KMicroblog::UserInfo::setDescription(const QString &description)
{
    m_description = description;
}

QString KMicroblog::UserInfo::description() const
{
    return m_description;
}

void KMicroblog::UserInfo::setFriendsCount(qulonglong count)
{
    m_friendsCount = count;
}

qulonglong KMicroblog::UserInfo::friendsCount() const
{
    return m_friendsCount;
}

void KMicroblog::UserInfo::setStatusesCount(qulonglong count)
{
    m_statusesCount = count;
}

qulonglong KMicroblog::UserInfo::statusesCount() const
{
    return m_statusesCount;
}

void KMicroblog::UserInfo::setFollowing(bool following)
{
    m_following = following;
}

bool KMicroblog::UserInfo::following() const
{
    return m_following;
}

void KMicroblog::UserInfo::setScreenName(const QString &screenName)
{
    m_screenName = screenName;
}

QString KMicroblog::UserInfo::screenName() const
{
    return m_screenName;
}
