/*
   Copyright 2012 Martin Klapetek <martin.klapetek@gmail.com>

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2 of the License or
   ( at your option ) version 3 or, at the discretion of KDE e.V.
   ( which shall act as a proxy as in section 14 of the GPLv3 ), any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "statusinfo.h"
#include <qjson/qobjecthelper.h>

void KMicroblog::StatusInfo::setId(const QString &id)
{
    m_id = id;
}

QString KMicroblog::StatusInfo::id() const
{
    return m_id;
}

void KMicroblog::StatusInfo::setText(const QString &text)
{
    m_text = text;
}

QString KMicroblog::StatusInfo::text() const
{
    return m_text;
}

void KMicroblog::StatusInfo::setCreatedAt (const QString &time)
{
    m_createdAt = time;
}

QString KMicroblog::StatusInfo::createdAtString() const
{
    return m_createdAt;
}

KDateTime KMicroblog::StatusInfo::createdAt() const
{
    return KDateTime::fromString(m_createdAt, "%a %b %d %k:%M:%S %z %Y"); //"Tue Jul 06 19:49:45 +0000 2010" //%Y-%m-%dT%H:%M:%S%z
}

void KMicroblog::StatusInfo::setTruncated(bool truncated)
{
    m_truncated = truncated;
}

bool KMicroblog::StatusInfo::truncated() const
{
    return m_truncated;
}

void KMicroblog::StatusInfo::setInReplyToStatusId(const QString &statusId)
{
    m_inReplyToStatusId = statusId;
}

QString KMicroblog::StatusInfo::inReplyToStatusId() const
{
    return m_inReplyToStatusId;
}

void KMicroblog::StatusInfo::setInReplyToUserId(const QString &userId)
{
    m_inReplyToUserId = userId;
}

QString KMicroblog::StatusInfo::inReplyToUserId() const
{
    return m_inReplyToUserId;
}

void KMicroblog::StatusInfo::setInReplyToScreenName(const QString &screenname)
{
    m_inReplyToScreenName = screenname;
}

QString KMicroblog::StatusInfo::inReplyToScreenName() const
{
    return m_inReplyToScreenName;
}

void KMicroblog::StatusInfo::setUser(const QVariantMap &map)
{
    m_user = KMicroblog::UserInfoPtr (new KMicroblog::UserInfo());
    QJson::QObjectHelper::qvariant2qobject(map, m_user.data());
}

KMicroblog::UserInfoPtr KMicroblog::StatusInfo::user() const
{
    return m_user;
}

QVariantMap KMicroblog::StatusInfo::userMap() const
{
    return QJson::QObjectHelper::qobject2qvariant(m_user.data());
}

void KMicroblog::StatusInfo::setGeo(const QString &geo)
{
    m_geo = geo;
}

QString KMicroblog::StatusInfo::geo() const
{
    return m_geo;
}

void KMicroblog::StatusInfo::setCoordinates(const QString &coords)
{
    m_coordinates = coords;
}

QString KMicroblog::StatusInfo::coordinates() const
{
    return m_coordinates;
}

void KMicroblog::StatusInfo::setPlace(const QString &place)
{
    m_place = place;
}

QString KMicroblog::StatusInfo::place() const
{
    return m_place;
}

void KMicroblog::StatusInfo::setContributors(const QString &contributors)
{
    m_contributors = contributors;
}

QString KMicroblog::StatusInfo::contributors() const
{
    return m_contributors;
}

void KMicroblog::StatusInfo::setRetweetCount(qulonglong count)
{
    m_retweetCount = count;
}

qulonglong KMicroblog::StatusInfo::retweetCount() const
{
    return m_retweetCount;
}

void KMicroblog::StatusInfo::setFavorited(bool favorited)
{
    m_favorited = favorited;
}

bool KMicroblog::StatusInfo::favorited() const
{
    return m_favorited;
}

void KMicroblog::StatusInfo::setRetweeted(bool retweeted)
{
    m_retweeted = retweeted;
}

bool KMicroblog::StatusInfo::retweeted() const
{
    return m_retweeted;
}

void KMicroblog::StatusInfo::setPossiblySensitive(bool possiblySensitive)
{
    m_possiblySensitive = possiblySensitive;
}

bool KMicroblog::StatusInfo::possiblySensitive() const
{
    return m_possiblySensitive;
}

void KMicroblog::StatusInfo::setRetweetedStatus(const QVariantMap &status)
{
    m_retweetedStatus = KMicroblog::StatusInfoPtr (new KMicroblog::StatusInfo());
    QJson::QObjectHelper::qvariant2qobject(status, m_retweetedStatus.data());
}

KMicroblog::StatusInfoPtr KMicroblog::StatusInfo::retweetedStatus() const
{
    return m_retweetedStatus;
}

QVariantMap KMicroblog::StatusInfo::retweetedStatusMap() const
{
    if (m_retweetedStatus.isNull()) {
        return QVariantMap();
    }

    return QJson::QObjectHelper::qobject2qvariant(m_retweetedStatus.data());
}
