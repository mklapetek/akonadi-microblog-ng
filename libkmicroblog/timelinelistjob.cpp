/*
   Copyright 2012 Martin Klapetek <martin.klapetek@gmail.com>

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2 of the License or
   ( at your option ) version 3 or, at the discretion of KDE e.V.
   ( which shall act as a proxy as in section 14 of the GPLv3 ), any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "timelinelistjob.h"
#include "statusinfo.h"

#include <Akonadi/SocialUtils/ImageProvider>

#include <KDebug>
#include <qjson/qobjecthelper.h>

KMicroblog::TimelineListJob::TimelineListJob(const QString &timelinePath, KOAuth::KOAuth *authHelper)
    : MicroblogGetJob(timelinePath, authHelper)
{
    kDebug() << "Creating job for" << timelinePath;
}

void KMicroblog::TimelineListJob::handleData(const QVariant& data)
{
//     kDebug() << "Processing" << data;

    Q_FOREACH(const QVariant &d, data.toList()) {
        StatusInfoPtr statusInfo(new StatusInfo());
        QJson::QObjectHelper::qvariant2qobject(d.toMap(), statusInfo.data());
        m_posts.append(statusInfo);
    }
}

QList<KMicroblog::StatusInfoPtr> KMicroblog::TimelineListJob::statuses() const
{
    return m_posts;
}

int KMicroblog::TimelineListJob::numEntries() const
{
    return m_posts.size();
}
