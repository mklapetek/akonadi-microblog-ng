/*
   Copyright 2012 Martin Klapetek <martin.klapetek@gmail.com>

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2 of the License or
   ( at your option ) version 3 or, at the discretion of KDE e.V.
   ( which shall act as a proxy as in section 14 of the GPLv3 ), any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef KMICROBLOG_STATUSINFO_H
#define KMICROBLOG_STATUSINFO_H

#include <QObject>
#include <QSharedPointer>
#include <QMetaType>
#include <QVariantMap>

#include <KDateTime>

#include "libkmicroblog_export.h"
#include "userinfo.h"

namespace KMicroblog {

class StatusInfo;
typedef QSharedPointer<StatusInfo> StatusInfoPtr;


class LIBKMICROBLOG_EXPORT StatusInfo : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString id WRITE setId READ id)
    Q_PROPERTY(QString text WRITE setText READ text)
    Q_PROPERTY(QString created_at WRITE setCreatedAt READ createdAtString)
    Q_PROPERTY(bool truncated WRITE setTruncated READ truncated)
    Q_PROPERTY(QString in_reply_to_status_id WRITE setInReplyToStatusId READ inReplyToStatusId)
    Q_PROPERTY(QString in_reply_to_user_id WRITE setInReplyToUserId READ inReplyToUserId)
    Q_PROPERTY(QString in_reply_to_screen_name WRITE setInReplyToScreenName READ inReplyToScreenName)
    Q_PROPERTY(QVariantMap user WRITE setUser READ userMap)
    Q_PROPERTY(QString geo WRITE setGeo READ geo)
    Q_PROPERTY(QString coordinates WRITE setCoordinates READ coordinates)
    Q_PROPERTY(QString place WRITE setPlace READ place)
    Q_PROPERTY(QString contributors WRITE setContributors READ contributors)
    Q_PROPERTY(qulonglong retweet_count WRITE setRetweetCount READ retweetCount)
    Q_PROPERTY(bool favorited WRITE setFavorited READ favorited)
    Q_PROPERTY(bool retweeted WRITE setRetweeted READ retweeted)
    Q_PROPERTY(bool possibly_sensitive WRITE setPossiblySensitive READ possiblySensitive)
    Q_PROPERTY(QVariantMap retweeted_status WRITE setRetweetedStatus READ retweetedStatusMap)

public:
    void setId(const QString &id);
    QString id() const;

    void setText(const QString &text);
    QString text() const;

    void setCreatedAt(const QString &time);
    QString createdAtString() const;
    KDateTime createdAt() const;

    void setTruncated(bool truncated);
    bool truncated() const;

    void setInReplyToStatusId(const QString &statusId);
    QString inReplyToStatusId() const;

    void setInReplyToUserId(const QString &userId);
    QString inReplyToUserId() const;

    void setInReplyToScreenName(const QString &screenname);
    QString inReplyToScreenName() const;

    void setUser(const QVariantMap &user);
    KMicroblog::UserInfoPtr user() const;
    QVariantMap userMap() const;

    void setGeo(const QString &geo);
    QString geo() const;

    void setCoordinates(const QString &coords);
    QString coordinates() const;

    void setPlace(const QString &place);
    QString place() const;

    void setContributors(const QString &contributors);
    QString contributors() const;

    void setRetweetCount(qulonglong count);
    qulonglong retweetCount() const;

    void setFavorited(bool favorited);
    bool favorited() const;

    void setRetweeted(bool retweeted);
    bool retweeted() const;

    void setPossiblySensitive(bool possiblySensitive);
    bool possiblySensitive() const;

    void setRetweetedStatus(const QVariantMap &status);
    KMicroblog::StatusInfoPtr retweetedStatus() const;
    QVariantMap retweetedStatusMap() const;

private:
    QString m_id;
    QString m_text;
    QString m_createdAt;
    bool m_truncated;
    QString m_inReplyToStatusId;
    QString m_inReplyToUserId;
    QString m_inReplyToScreenName;
    KMicroblog::UserInfoPtr m_user;
    QString m_geo;
    QString m_coordinates;
    QString m_place;
    QString m_contributors;
    qulonglong m_retweetCount;
    bool m_favorited;
    bool m_retweeted;
    bool m_possiblySensitive;
    KMicroblog::StatusInfoPtr m_retweetedStatus;
};


}

Q_DECLARE_METATYPE(KMicroblog::StatusInfo *);

#endif // KMICROBLOG_STATUSINFO_H
