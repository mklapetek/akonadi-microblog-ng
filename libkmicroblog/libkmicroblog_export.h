#ifndef LIBKMICROBLOG_EXPORT_H
#define LIBKMICROBLOG_EXPORT_H

/* needed for KDE_EXPORT and KDE_IMPORT macros */
#include <kdemacros.h>

#ifndef LIBKMICROBLOG_EXPORT
# if defined(MAKE_LIBKMICROBLOG_LIB)
/* We are building this library */
#  define LIBKMICROBLOG_EXPORT KDE_EXPORT
# else
/* We are using this library */
#  define LIBKMICROBLOG_EXPORT KDE_IMPORT
# endif
#endif

#endif
