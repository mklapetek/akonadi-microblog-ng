/*
   Copyright 2012 Martin Klapetek <martin.klapetek@gmail.com>

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2 of the License or
   ( at your option ) version 3 or, at the discretion of KDE e.V.
   ( which shall act as a proxy as in section 14 of the GPLv3 ), any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/


#ifndef KMICROBLOG_MICROBLOGJOB_H
#define KMICROBLOG_MICROBLOGJOB_H

#include <KJob>
#include <QStringList>
#include <libkoauth/koauth.h>

#include "libkmicroblog_export.h"

namespace KIO {
class TransferJob;
}

typedef QPair<QString, QString> QueryItem;

namespace QCA
{
    class Initializer;
}

namespace KMicroblog {

class LIBKMICROBLOG_EXPORT MicroblogJob : public KJob
{
    Q_OBJECT
public:
    /**
     * Constructor that sets the path and the accesstoken
     *
     * @param path The path after https://graphs.facebook.com
     * @param accessToken The accessToken to access our data on facebook
     * */
    MicroblogJob(const QString &path, KOAuth::KOAuth *authHelper);
    //virtual ~MicroblogJob();
//    explicit MicroblogJob( const QString &accessToken );

    /** Add a query item to the list */
    void addQueryItem(const QByteArray &key, const QByteArray &value);

    virtual void start() = 0;

    enum JobErrorType { AuthenticationProblem = KJob::UserDefinedError + 42 };

protected:
    /** Kill the currentjobs and its subjobs */
    virtual bool doKill();

    /** Check for a return error and set the appropiate error messags */
    void handleError(const QVariant &data);

    QString m_path;                                 /** path after https://api.twitter.com/1/ */
    QWeakPointer<KIO::TransferJob> m_job;           /** Pointer to the running job */

    KUrl m_url;
    KUrl m_serviceBaseUrl;
    QByteArray m_jobData;

    QString m_id;
    QOAuth::ParamMap m_params;

    KOAuth::KOAuth *m_authHelper;

private Q_SLOTS:
    virtual void jobFinished( KJob *job ) = 0;
};

//=======================================================================================

/**
 * MicroblogJob that gets data from service
 */
class LIBKMICROBLOG_EXPORT MicroblogGetJob : public MicroblogJob
{
    Q_OBJECT

public:
    MicroblogGetJob( const QString &path, KOAuth::KOAuth *authHelper );
//    explicit MicroblogGetJob( const QString &accessToken );

    /** Set the fields the job should retrieve from microblog */
    void setFields( const QStringList &fields );

    /** Set the Id's the job should retrieve from microblog.
     * If this is set then the path is ignored */
    void setIds( const QStringList &ids );

    virtual void start();

protected:
    virtual void handleData( const QVariant &data ) = 0;

protected Q_SLOTS:
    void jobFinished( KJob *job );

private Q_SLOTS:
    void recv( KIO::Job*, const QByteArray &data );

private:
    QStringList mFields; /** The field to retrieve from microblog*/
    QStringList mIds;    /** The id's to retrieve from microblog*/
};

//=======================================================================================

/**
 * MicroblogJob that gets data from service
 */
class LIBKMICROBLOG_EXPORT MicroblogAddJob : public MicroblogJob
{
    Q_OBJECT

public:
    MicroblogAddJob( const QString &path, KOAuth::KOAuth *authHelper );

    virtual void start();

private Q_SLOTS:
    void recv( KIO::Job*, const QByteArray &data );
    void jobFinished( KJob *job );
};

}

#endif // KMICROBLOG_MICROBLOGJOB_H
