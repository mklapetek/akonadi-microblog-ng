/*
   Copyright 2012 Martin Klapetek <martin.klapetek@gmail.com>

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2 of the License or
   ( at your option ) version 3 or, at the discretion of KDE e.V.
   ( which shall act as a proxy as in section 14 of the GPLv3 ), any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/


#include "microblogjob.h"
#include <KDebug>
#include <KLocalizedString>
#include <KUrl>
#include <KIO/Job>
#include <qjson/parser.h>

KMicroblog::MicroblogJob::MicroblogJob(const QString& path, KOAuth::KOAuth *authHelper)
    : m_path(path),
      m_authHelper(authHelper)
{
    setCapabilities(KJob::Killable);

    m_url = KUrl(KUrl(m_authHelper->serviceBaseUrl()), m_path.append(".json"));
}

void KMicroblog::MicroblogJob::addQueryItem(const QByteArray &key, const QByteArray &value)
{
    m_params.insert(key, value);
}

void KMicroblog::MicroblogJob::handleError(const QVariant& data)
{
    const QVariantMap errorMap = data.toMap();
    const QString type = errorMap["type"].toString();
    const QString message = errorMap["message"].toString();
    kWarning() << "An error of type" << type << "occurred:" << message;
    if ( type.toLower() != "oauthexception" ) {
        setError( KJob::UserDefinedError );
        setErrorText( i18n( "The server returned an error of type <i>%1</i>: <i>%2</i>" , type, message ) );
    } else {
        setError( AuthenticationProblem );
        setErrorText( i18n( "Unable to login to the server, authentication failure.\nThe server said: <i>%1</i>", message ) );
    }
}

bool KMicroblog::MicroblogJob::doKill()
{
    if (!m_job.isNull()) {
        m_job.data()->kill(KJob::Quietly);
    }
    return KJob::doKill();
}

/*************************************************/

KMicroblog::MicroblogGetJob::MicroblogGetJob(const QString& path, KOAuth::KOAuth *authHelper)
    : MicroblogJob(path, authHelper)
{

}

void KMicroblog::MicroblogGetJob::setFields(const QStringList &fields)
{
    mFields = fields;
}

void KMicroblog::MicroblogGetJob::setIds(const QStringList &ids)
{
    mIds = ids;
}

void KMicroblog::MicroblogGetJob::start()
{
    kDebug();
    if (!m_authHelper->isAuthorized()) {
        kDebug() << "Not authorized, returning...";
        return;
    }

    //if there are no params set, use the defaults
    if (m_params.isEmpty()) {
        m_params.insert("include_entities", "true");
        m_params.insert("include_rts", "true");
        m_params.insert("count", "20");
        m_params.insert("trim_user", "false");
    }

    //append the parameters to the url
    KUrl url = KUrl(m_url.pathOrUrl().append(m_authHelper->userParameters(m_params)));
    kDebug() << "Creating job..." << m_url << "User: " << m_authHelper->user();

    // Create a KIO job to get the data from the web service
    m_job = KIO::get(url, KIO::Reload, KIO::HideProgressInfo);
    //sign the request with the original url (without the params)
    m_authHelper->sign(m_job.data(), m_url.pathOrUrl(), m_params);

    connect(m_job.data(), SIGNAL(data(KIO::Job*,QByteArray)),
            this, SLOT(recv(KIO::Job*,QByteArray)));
    connect(m_job.data(), SIGNAL(result(KJob*)), this, SLOT(jobFinished(KJob*)));

    m_job.data()->start();
}

void KMicroblog::MicroblogGetJob::jobFinished(KJob* job)
{
    KIO::TransferJob *tj = dynamic_cast<KIO::TransferJob*>(job);
    if (!job || job != m_job.data()) {
        kDebug() << "#fail job is not our job!";
        return;
    }

    if (job->error() || tj->isErrorPage()) {
        kDebug() << "Timeline job error: " << job->errorString() << tj->url();
        // TODO: error handling
    } else {
        kDebug() << "Timeline job returned: " << tj->url();
//         kDebug() << m_jobData;//.data();
        QJson::Parser parser;
        bool ok;
        const QVariant data = parser.parse( m_jobData, &ok );
        if ( ok ) {
            const QVariant error = data.toMap()["errors"];
            if ( error.isValid() ) {
                handleError( error );
            } else {
                handleData( data );
            }
        } else {
            kWarning() << "Unable to parse JSON data: " << m_jobData.data();
            setError( KJob::UserDefinedError );
            setErrorText( i18n( "Unable to parse data returned by the server: %1", parser.errorString() ) );
        }
    }

    m_jobData.clear();
    emitResult();
    m_job.data()->deleteLater();
}

void KMicroblog::MicroblogGetJob::recv(KIO::Job* , const QByteArray& data)
{
    m_jobData += data;
}

//=======================================================================================

KMicroblog::MicroblogAddJob::MicroblogAddJob( const QString &path, KOAuth::KOAuth *authHelper )
  : KMicroblog::MicroblogJob( path, authHelper )
{
    m_params.insert("source", "kdemicroblog");
}

void KMicroblog::MicroblogAddJob::start()
{
    QByteArray data = m_authHelper->userParameters(m_params, QOAuth::ParseForRequestContent);

    m_job = KIO::http_post(m_url, data, KIO::HideProgressInfo);
    m_job.data()->addMetaData("content-type", "application/x-www-form-urlencoded");

    m_authHelper->sign(m_job.data(), m_url.pathOrUrl(), m_params, QOAuth::POST);

    connect(m_job.data(), SIGNAL(data(KIO::Job*,QByteArray)),
            this, SLOT(recv(KIO::Job*,QByteArray)));
    connect(m_job.data(), SIGNAL(result(KJob*)), this, SLOT(jobFinished(KJob*)));

    m_job.data()->start();
}

void KMicroblog::MicroblogAddJob::recv( KIO::Job*, const QByteArray &data )
{
    m_jobData += data;
}
void KMicroblog::MicroblogAddJob::jobFinished( KJob *job )
{
    KIO::TransferJob *tj = dynamic_cast<KIO::TransferJob*>(job);
    if (!job || job != m_job.data()) {
        kDebug() << "#fail job is not our job!";
        return;
    }

    if (job->error()) {
        kDebug() << "Status update job error: " << job->errorString() << tj->url();
        // TODO: error handling
    } else {
        kDebug() << "Status update job returned: " << tj->url();
        kDebug() << m_jobData;//.data();
        QJson::Parser parser;
        bool ok;
        const QVariant data = parser.parse( m_jobData, &ok );
        //         kDebug() << "Received data" << data;
        if ( ok ) {
            const QVariant error = data.toMap()["error"];
            if ( error.isValid() ) {
                handleError( error );
            } else {
                kDebug() << "Status posted";
            }
        } else {
            kWarning() << "Unable to parse JSON data: " << m_jobData.data();
            setError( KJob::UserDefinedError );
            setErrorText( i18n( "Unable to parse data returned by the server: %1", parser.errorString() ) );
        }
    }

    m_jobData.clear();
    emitResult();
    m_job.data()->deleteLater();
}