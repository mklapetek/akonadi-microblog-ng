/*
   Copyright 2012 Martin Klapetek <martin.klapetek@gmail.com>

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2 of the License or
   ( at your option ) version 3 or, at the discretion of KDE e.V.
   ( which shall act as a proxy as in section 14 of the GPLv3 ), any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "microblogresource.h"

#include "settings.h"
#include "settingsadaptor.h"
#include "libkmicroblog/timelinelistjob.h"
#include "libkmicroblog/statusaddjob.h"
#include "settingsdialog.h"

#include <QtDBus/QDBusConnection>

#include <KLocalizedString>
#include <KDialog>
#include <KDebug>

#include <Akonadi/EntityDisplayAttribute>
#include <Akonadi/AttributeFactory>
#include <Akonadi/ItemFetchScope>
#include <Akonadi/ChangeRecorder>

#include <qjson/qobjecthelper.h>
#include <Akonadi/SocialUtils/ImageProvider>
#include <Akonadi/SocialUtils/SocialNetworkAttributes>

using namespace Akonadi;

static const QLatin1String timelineRID( "timeline" );
static const QLatin1String mentionsRID( "mentions" );


MicroblogResource::MicroblogResource( const QString &id )
  : ResourceBase( id )
{
  new SettingsAdaptor( Settings::self() );
  QDBusConnection::sessionBus().registerObject( QLatin1String( "/Settings" ),
                            Settings::self(), QDBusConnection::ExportAdaptors );

  setNeedsNetwork( true );
  setObjectName( QLatin1String( "MicroblogResource" ) );

  m_authHelper = new KOAuth::KOAuth( this );
  m_authHelper.data()->init();

  connect( this, SIGNAL( abortRequested() ),
           this, SLOT( onAbortRequested() ) );

  m_avatarProvider = new ImageProvider( this );

  if ( !Settings::serviceBaseUrl().isEmpty() && !Settings::username().isEmpty() ) {
      m_authHelper.data()->setServiceBaseUrl( Settings::serviceBaseUrl() );
      m_authHelper.data()->setUser( Settings::username() );

      kDebug() << "Resource has been init'd for" << Settings::username() << "@" << Settings::serviceBaseUrl();
  }
  changeRecorder()->itemFetchScope().fetchFullPayload( true );
  AttributeFactory::registerAttribute<SocialNetworkAttributes>();

  QDBusConnection::sessionBus().registerObject(QLatin1String("/org/kde/mklapetek"), this, QDBusConnection::ExportAllContents);

}

MicroblogResource::~MicroblogResource()
{
}

void MicroblogResource::retrieveCollections()
{
  // TODO: this method is called when Akonadi wants to have all the
  // collections your resource provides.
  // Be sure to set the remote ID and the content MIME types

  QString userName = Settings::username();
  bool isTwitter = Settings::serviceBaseUrl().contains( "twitter.com" );

  Collection timeline;
  timeline.setRemoteId( timelineRID );

  if (isTwitter) {
    timeline.setName( i18n( "Twitter Timeline (%1)" ).arg( userName ) );
  } else {
    timeline.setName( i18n( "Identi.ca Timeline (%1)" ).arg( userName ) );
  }
  timeline.setParentCollection( Akonadi::Collection::root() );
  timeline.setContentMimeTypes( QStringList() << "text/x-vnd.akonadi.socialfeeditem" );
  timeline.setRights( Collection::CanCreateItem );
  EntityDisplayAttribute * const postsDisplayAttribute = new EntityDisplayAttribute();
  postsDisplayAttribute->setIconName( "microblogresource" );
  timeline.addAttribute( postsDisplayAttribute );

  SocialNetworkAttributes * const socialAttribs = new SocialNetworkAttributes( userName,
                                                                               isTwitter ? "Twitter" : "Identi.ca",
                                                                               true,
                                                                               140);
  timeline.addAttribute( socialAttribs );

  Collection mentions;
  mentions.setRemoteId( mentionsRID );
  if (isTwitter) {
      mentions.setName( i18n( "Mentions on Twitter (%1)" ).arg( userName ) );
  } else {
      mentions.setName( i18n( "Mentions on Identi.ca (%1)" ).arg( userName ) );
  }
  mentions.setParentCollection( Akonadi::Collection::root() );
  mentions.setContentMimeTypes( QStringList() << "text/x-vnd.akonadi.socialfeeditem" );
  mentions.setRights( Collection::ReadOnly );
  EntityDisplayAttribute * const mentionsDisplayAttribute = new EntityDisplayAttribute();
  mentionsDisplayAttribute->setIconName( "microblogresource" );
  mentions.addAttribute( mentionsDisplayAttribute );

  collectionsRetrieved( Collection::List() << timeline << mentions );
}

void MicroblogResource::retrieveItems( const Akonadi::Collection &collection )
{
  // TODO: this method is called when Akonadi wants to know about all the
  // items in the given collection. You can but don't have to provide all the
  // data for each item, remote ID and MIME type are enough at this stage.
  // Depending on how your resource accesses the data, there are several
  // different ways to tell Akonadi when you are done.

    m_idle = false;
    emit status( Running, i18n( "Preparing sync of timeline." ) );
    emit percent( 0 );
    kDebug() << "Getting timeline";
    KMicroblog::TimelineListJob *timelineJob = 0;

    if ( collection.remoteId() == timelineRID ) {
        timelineJob = new KMicroblog::TimelineListJob( "statuses/home_timeline", m_authHelper.data() );
    } else if ( collection.remoteId() == mentionsRID ) {
        timelineJob = new KMicroblog::TimelineListJob( "statuses/mentions", m_authHelper.data() );
    }

    if ( timelineJob == 0 ) {
        return;
    }

    m_currentJobs << QWeakPointer<KJob>( timelineJob );
    connect(timelineJob, SIGNAL( finished( KJob* ) ), this, SLOT( timelineJobFinished( KJob* ) ) );
    timelineJob->start();
}

bool MicroblogResource::retrieveItem( const Akonadi::Item &item, const QSet<QByteArray> &parts )
{
  Q_UNUSED( item );
  Q_UNUSED( parts );

  // TODO: this method is called when Akonadi wants more data for a given item.
  // You can only provide the parts that have been requested but you are allowed
  // to provide all in one go

  return true;
}

void MicroblogResource::aboutToQuit()
{
  onAbortRequested();
}

void MicroblogResource::configure( WId windowId )
{
  SettingsDialog *dialog = new SettingsDialog( this, m_authHelper.data(), windowId );
  if ( dialog->exec() == KDialog::Accepted ) {
      emit configurationDialogAccepted();
      //start syncing right after the config is done
      synchronize();
  } else {
      emit configurationDialogRejected();
  }

  delete dialog;
}

void MicroblogResource::configureByAccount( int accountId )
{

}

void MicroblogResource::itemAdded( const Akonadi::Item &item, const Akonadi::Collection &collection )
{
    if ( collection.remoteId() == timelineRID ) {
        QString message;
        if ( item.hasPayload<SocialFeedItem>() ) {
            message = item.payload<SocialFeedItem>().postText();
            m_idle = false;
        } else {
            kDebug() << "Wrong payload!";
            return;
        }

        if (!message.isEmpty()) {
            KMicroblog::StatusAddJob * const addJob = new KMicroblog::StatusAddJob( message, m_authHelper.data() );
            m_currentJobs << addJob;
            addJob->setProperty( "Item", QVariant::fromValue( item ));
            connect( addJob, SIGNAL(result(KJob*)), this, SLOT(statusAddJobFinished(KJob*) ) );
            emit status( Running, i18n( "Updating status..." ) );
            addJob->start();
        }
    }
  // TODO: this method is called when somebody else, e.g. a client application,
  // has created an item in a collection managed by your resource.

  // NOTE: There is an equivalent method for collections, but it isn't part
  // of this template code to keep it simple
}

void MicroblogResource::itemChanged( const Akonadi::Item &item, const QSet<QByteArray> &parts )
{
  Q_UNUSED( item );
  Q_UNUSED( parts );

  // TODO: this method is called when somebody else, e.g. a client application,
  // has changed an item managed by your resource.

  // NOTE: There is an equivalent method for collections, but it isn't part
  // of this template code to keep it simple
}

void MicroblogResource::itemRemoved( const Akonadi::Item &item )
{
  Q_UNUSED( item );

  // TODO: this method is called when somebody else, e.g. a client application,
  // has deleted an item managed by your resource.

  // NOTE: There is an equivalent method for collections, but it isn't part
  // of this template code to keep it simple
}

void MicroblogResource::timelineJobFinished( KJob *job )
{
//     Q_ASSERT( !mIdle );
    KMicroblog::TimelineListJob * const listJob = dynamic_cast<KMicroblog::TimelineListJob*>( job );
    Q_ASSERT( listJob );
    m_currentJobs.removeAll( job );

    if ( listJob->error() ) {
        kWarning() << "Something bad happened:" << listJob->errorString();
    } else {
        setItemStreamingEnabled( true );

        Item::List statusItems;
        kDebug() << "Going into foreach";
        foreach( const KMicroblog::StatusInfoPtr &statusInfo, listJob->statuses() ) {
            Item status;
            status.setRemoteId( statusInfo.data()->id() );
            status.setMimeType( "text/x-vnd.akonadi.socialfeeditem" );
            status.setPayload<SocialFeedItem>( convertToSocialFeedItem( statusInfo ) );
            statusItems.append( status );
            m_avatarProvider->loadImage( statusInfo->user()->screenName(), statusInfo->user()->profileImageUrl(), true );
        }

        itemsRetrieved( statusItems );
        itemsRetrievalDone();
        emit percent( 100 );
        emit status( Idle, i18n( "All statuses fetched from server." ) );
        m_idle = true;
    }
}

void MicroblogResource::statusAddJobFinished( KJob *job )
{
    KMicroblog::StatusAddJob * const addJob = dynamic_cast<KMicroblog::StatusAddJob*>( job );
    Q_ASSERT( addJob );
    m_currentJobs.removeAll(job);

    if (job->error()) {
        kDebug() << "Error updating status:" << job->errorString();
    } else {
        Item post = addJob->property( "Item" ).value<Item>();
        post.setRemoteId(addJob->property( "id" ).value<QString>());
        changeCommitted( post );
        kDebug() << "Status posted to server";
        m_idle = true;
        emit status( Idle, i18n( "Status updated." ) );
    }
}

void MicroblogResource::onAbortRequested()
{
    if ( !m_idle ) {
        foreach( const QWeakPointer<KJob> &job, m_currentJobs ) {
            kDebug() << "Killing current job:" << job;
            if ( !job.isNull() ) {
                job.data()->kill( KJob::Quietly );
            }
        }
        cancelTask();
        m_avatarProvider->abortAllJobs();
        m_idle = true;
    }
}

SocialFeedItem MicroblogResource::convertToSocialFeedItem( const KMicroblog::StatusInfoPtr &statusInfo )
{
    SocialFeedItem item;
    item.setPostId( statusInfo.data()->id() );

    if ( !statusInfo.data()->retweetedStatus().isNull() ) {
        item.setPostText( statusInfo.data()->retweetedStatus().data()->text() );
        item.setPostTime( statusInfo.data()->createdAtString(), QLatin1String( "%a %b %d %k:%M:%S %z %Y" ) );
        item.setShared( true );
        item.setSharedFrom( statusInfo.data()->retweetedStatus().data()->user().data()->name() );
    } else {
        item.setPostText( statusInfo.data()->text() );
        item.setPostTime( statusInfo.data()->createdAtString(), QLatin1String( "%a %b %d %k:%M:%S %z %Y" ) );
        item.setShared( false );
    }

    KMicroblog::UserInfoPtr user;

    if ( !statusInfo.data()->retweetedStatus().isNull() ) {
        user = statusInfo.data()->retweetedStatus().data()->user();
        item.setPostInfo( i18n( "Retweeted by %1", statusInfo.data()->user().data()->name() ) );
    } else {
        user = statusInfo.data()->user();
    }

    item.setUserId( user.data()->id() );
    item.setUserName( user.data()->screenName() );
    item.setUserDisplayName( user.data()->name() );
    item.setAvatarUrl( user.data()->profileImageUrl().toString() );

    if ( Settings::serviceBaseUrl().contains( "twitter.com" ) ) {
        item.setNetworkString( i18nc( "This string is used in a sentence 'Someone on Twitter: Just had lunch.', so should be translated in such form."
                                      "This string is defined by the resource and the whole sentence is composed in the UI.",
                                      "on Twitter" ) );
    } else {
        item.setNetworkString( i18nc( "This string is used in a sentence 'Someone on Identi.ca: Just had lunch.', so should be translated in such form."
                                      "This string is defined by the resource and the whole sentence is composed in the UI.",
                                      "on Identi.ca" ) );
    }

    item.setItemSourceMap( QJson::QObjectHelper::qobject2qvariant( statusInfo.data() ) );

    return item;
}

AKONADI_RESOURCE_MAIN( MicroblogResource )

#include "microblogresource.moc"
